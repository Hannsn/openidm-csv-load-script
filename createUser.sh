

function create_user() {

  cmd="curl -k --location --request POST $openidm'/openidm/managed/user?_action=create' \
  --header 'X-OpenIDM-Password: openidm-admin' \
  --header 'X-OpenIDM-Username: openidm-admin' \
  --header 'Content-Type: application/json' \
  --data-raw '{
    "_id": "$id",
    "userName": "$userName",
    "givenName": "$givenName",
    "sn": "$sn",
    "mail": "$mail",
    "accountStatus": "$accountStatus",
    "userType": "$userType",
    "description": "$description",
    "telephoneNumber": "$telephoneNumber",
    "postalAddress": "$postalAddress",
    "city": "$city",
    "country": "$country",
    "postalCode": "$postalCode",
    "stateProvince": "$stateProvince",
    "company": "$company",
    "password": "$password"
  }'"

eval "$cmd"

}

IFS=","
file="users.csv"
num_lines=$(cat $file | wc -l)
openidm="https://localhost:8443"
openidm_user="openidm-admin"
openidm_pwd="Passw0rd"
i=1

[ ! -f $file ] && { echo "$file file not found"; exit 99; }

while read -r id userName givenName sn mail updates marketing accountStatus userType description telephoneNumber postalAddress city stateProvince company postalCode country password
do
        test $i -eq 1 && ((i=i+1)) && continue
        echo "Creating user  : $id"
        echo "userName       : $userName"
        echo "givenName      : $givenName"
        echo "sn             : $sn"
        echo "mail           : $mail"
        echo "updates        : $updates"
        echo "marketing      : $marketing"
        echo "accountStatus  : $accountStatus"
        echo "userType       : $userType"
        echo "description    : $description"
        echo "telephoneNumber: $telephoneNumber"
        echo "postalAddress  : $postalAddress"
        echo "city           : $city"
        echo "stateProvince  : $stateProvince"
        echo "company        : $company"

        create_user

done < $file
