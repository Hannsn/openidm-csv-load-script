# Demo script to load users from csv into OpenIDM
This is a simple shell script to leverage the idm rest api to load users (here managed/users) into idm repo.

# Disclaimer
The sample code described herein is provided on an "as is" basis, without warranty of any kind, to the fullest extent permitted by law. ForgeRock does not warrant or guarantee the individual success developers may have in implementing the sample code on their development platforms or in production configurations.

ForgeRock does not warrant, guarantee or make any representations regarding the use, results of use, accuracy, timeliness or completeness of any data or information relating to the sample code. ForgeRock disclaims all warranties, expressed or implied, and in particular, disclaims all warranties of merchantability, and warranties related to the code, or any service or software related thereto.

ForgeRock shall not be liable for any direct, indirect or consequential damages or costs of any type arising out of any action taken by you or others related to the sample code.

# Purpose
Often I need just demo data or test data. Of course you could just configure csv connector to sync data from csv to idm repo. But sometimes you just need demo data to quickly test idm use cases.

So I hope this becomes handy.

[Question? hanns](mailto:hanns.nolan@forgerock.com)
